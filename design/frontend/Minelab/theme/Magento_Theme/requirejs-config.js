var config = {
    paths: {
        'bootstrap': 'Magento_Theme/js/bootstrap.min',
        maskedinput: 'Magento_Theme/js/jquery.maskedinput.min'
    } ,
    shim: {
        'bootstrap': {
            'deps': ['jquery']
        }
    }
};