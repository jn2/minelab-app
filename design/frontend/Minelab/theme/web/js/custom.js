requirejs(['jquery', 'jquery/ui', 'bootstrap', 'maskedinput'], function ($, jQueryBootstrap) {

    if(typeof $('.dropdown-toggle').dropdown === "function")
        $('.dropdown-toggle').dropdown();

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            },
            clearIfNotMatch: true
        };
    $('input[name="telephone"]').mask(SPMaskBehavior, spOptions);


    $('input[name="postcode"],#simularFrete .field.postcode .control input#cep').mask('00000-000', {
        onComplete: function(cep) {
            //alert('CEP Completed!:' + cep);
        }
    });

    $('input[name="taxvat"]').mask('000.000.000-00', { clearIfNotMatch: true });
});

var geral = {
    funcs: {
        scrollToElement: function(selector, duration) {
            jQuery('html, body').animate({
                scrollTop: jQuery(selector).offset().top },
                duration || 1500
            );
        }
    }
};