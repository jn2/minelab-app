requirejs(['jquery'], function ($) {
    var intervalId = setInterval(function(){
        if($('input[name="postcode"]').length) {

            $('input[name="postcode"]').closest('.control').append('<button onclick="location.reload()" class="btn-recalcular-frete" disabled>Recalcular</button>')

            var lastCEPVerifify = $('input[name="postcode"]').val().length == 9 ? $('input[name="postcode"]').val() : null;

            $('input[name="postcode"]').on('keyup', function(){
                $('.btn-recalcular-frete').prop('disabled', ! ($(this).val().length == 9 && (lastCEPVerifify == null || $(this).val() != lastCEPVerifify)));

                if($(this).val().length == 9 && (lastCEPVerifify == null || $(this).val() == lastCEPVerifify))
                    $('#shipping-method-buttons-container').fadeIn();
                else
                    $('#shipping-method-buttons-container').fadeOut();
            });

            clearInterval(intervalId);
        }
    }, 150);
});
