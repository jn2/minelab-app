var _dealers = {
    "MA": {
        "Maracaçumé": [
            {
                "name":             'Comam — Maracaçumé',
                "street_address":   "Avenida Dayse, S/N",
                "city":             "Maracaçumé",
                "state":            "Maranhão",
                "state_abbreviation": "MA",
                "post_code":        "65289-000",
                "latitude":         "-2.045859",
                "longitude":        "-45.959949",
                "phones":           ["(98) 9847 72638"],
                "whatsapp":         [],
                "website":          "",
                "email":            ""
            }
        ]
    },
    "MT": {
        "Pontes e Lacerda": [
            {
                "name":             "Agua Facil",
                "street_address":   "Avenida Mal. Rondon, 1719 - Centro",
                "city":             "Pontes e Lacerda",
                "state":            "Mato Grosso",
                "state_abbreviation": "MT",
                "post_code":        "78250-000",
                "latitude":         "-15.26391",
                "longitude":        "-59.323357",
                "phones":           ["(65) 3266-1320", "(65) 9996 31749"],
                "whatsapp":         [],
                "website":          "",
                "email":            ""
            }
        ],
        "Varzea Grande": [
            {
                "name":             "Procria",
                "street_address":   "Av. Governador Julio Campos, 3548",
                "city":             "Varzea Grande",
                "state":            "Mato Grosso",
                "state_abbreviation": "MT",
                "post_code":        "78140-000",
                "latitude":         "-15.64762",
                "longitude":        "-56.157313",
                "phones":           ["(65) 3684 2412"],
                "whatsapp":         [],
                "website":          "",
                "email":            ""
            }
        ]
    },
    "SC": {
        "Navegantes": [
            {
                "name":             "DM Detectores",
                "street_address":   "Av. Prefeito Cirino Adolfo Cabral, 7835",
                "city":             "Navegantes",
                "state":            "Santa Catarina",
                "state_abbreviation": "SC",
                "post_code":        "88375-000",
                "latitude":         "",
                "longitude":        "",
                "phones":           ["(47) 99912-8091"],
                "whatsapp":         [],
                "website":          "",
                "email":            ""
            }
        ],
        "Joinville": [
            {
                "name":             "Fortuna Detectores de Metais",
                "street_address":   "Rua Nova Trento 341 – Bom Retiro",
                "city":             "Joinville",
                "state":            "Santa Catarina",
                "state_abbreviation": "SC",
                "post_code":        "89222-510",
                "latitude":         "",
                "longitude":        "",
                "phones":           [],
                "whatsapp":         [],
                "website":          "",
                "email":            ""
            }
        ]
    },
    "AM": {
        "Manus": [
            {
                "name":             "Moto Norte - Manus",
                "street_address":   "Rua Miranda Leao, 421 - Centro",
                "city":             "Manus",
                "state":            "Amazonas",
                "state_abbreviation": "AM",
                "post_code":        "69005-040",
                "latitude":         "-3.138882",
                "longitude":        "-60.022599",
                "phones":           ["(92) 2123-0950", "(92) 9913 44257"],
                "whatsapp":         [],
                "website":          "https://www.motonorte.com.br/",
                "email":            ""
            }
        ]
    },
    "RJ": {
        "Rio de Janeiro": [
            {
                "name":             "Oasis Detectores",
                "street_address":   "Avenida Almirante Barroso, 06 - sala 1308 Centro",
                "city":             "Rio de Janeiro",
                "state":            "Rio de Janeiro",
                "state_abbreviation": "RJ",
                "post_code":        "20031-002",
                "latitude":         "-22.908246",
                "longitude":        "-43.177646",
                "phones":           ["+55 (21) 3040 0226", "(94) 99224 - 0055", "(21) 99861 - 8922"],
                "whatsapp":         [],
                "website":          "http://oasisdetectores.com.br/",
                "email":            "contato@oasisdetectores.com.br"
            }
        ]
    },
    "PR": {
        "Belém": [
            {
                "name":             "MotoNorte - Belém",
                "street_address":   "Siqueira Mendes, 107 - Cidade Velha",
                "city":             "Belém",
                "state":            "Para",
                "state_abbreviation": "PR",
                "post_code":        "66020-600",
                "latitude":         "-1.4568661",
                "longitude":        "-48.50568138",
                "phones":           ["(91) 2121-6386"],
                "whatsapp":         [],
                "website":          "",
                "email":            ""
            }
        ],
        "Santarem": [
            {
                "name":             "MotoNorte - Santarém",
                "street_address":   "Pc Tiradentes, 28 - Aldeia",
                "city":             "Santarem",
                "state":            "Para",
                "state_abbreviation": "PR",
                "post_code":        "68040-240",
                "latitude":         "-1.727851",
                "longitude":        "-48.887982",
                "phones":           ["(93) 3063-5526", "(93) 9911 21585"],
                "whatsapp":         [],
                "website":          "",
                "email":            ""
            }
        ]
    },
    "GO": {
        "Uruacu": [
            {
                "name":             "Gold Detectores de Metal",
                "street_address":   "Av. Tocantins, 71, QD 42, LT 134",
                "city":             "Uruacu",
                "state":            "Goias",
                "state_abbreviation": "GO",
                "post_code":        "76.400-000",
                "latitude":         "-14.525984",
                "longitude":        "-49.138795",
                "phones":           ["(62) 3357-1694", "(62) 33571693", "0800 648 1694"],
                "whatsapp":         ["(62) 98165-5175 - TIM", "(62) 99967-2671 - VIVO"],
                "website":          "http://www.loja.golddetectores.com.br/",
                "email":            "contato@golddetectores.com.br"
            }
        ]
    },
    "RO": {
        "Boa Vista": [
            {
                "name":             "Casa do Paraiba",
                "street_address":   "Avenida Ville Roy, 7405",
                "city":             "Boa Vista",
                "state":            "Roraima",
                "state_abbreviation": "RO",
                "post_code":        "69303-445",
                "latitude":         "2.813068",
                "longitude":        "-60.679521",
                "phones":           ["(95) 3224-6824", "(95) 9913 86686"],
                "whatsapp":         [],
                "website":          "",
                "email":            ""
            }
        ]
    }
};

window.onload = function(){

    if(dealers == undefined)
    {
        console.error('A variavel com as lojas não está definida.');

        return;
    }

    var selectState = jQuery('.find-a-store form select[name=state]');
    var storesContainerList = jQuery('.find-a-store .stores-container .stores-list');

    jQuery(selectState).on('change', function() {
        var selected = jQuery(this).val();

        jQuery(storesContainerList).fadeOut(200, function(){

            var stores = '';

            jQuery.each(dealers[selected], function (index, _dealers) {
                jQuery.each(_dealers, function (index, dealer) {
                    var links = dealer.latitude != null && dealer.longitude != null && dealer.latitude.length > 0 && dealer.longitude.length ? '<a href="https://www.google.com/maps?q=' + dealer.latitude +  ',' + dealer.longitude + '" class="link" target="_blank"><span class="glyphicon glyphicon-map-marker"></span> Ver no mapa</a>' : '';
                    links += ((dealer.website != null && dealer.website.length > 0) ? '<a href="' + dealer.website + '" class="link" target="_blank"><span class="glyphicon glyphicon-bookmark"></span> Ver o site</a>' : '');
                    links += ((dealer.email != null && dealer.email.length > 0 ) ? '<a href="mailto:' + dealer.email + '" class="link" target="_blank"><span class="glyphicon glyphicon-envelope"></span> ' + dealer.email + '</a>' : '');

                    stores += '<div class="col-md-6"><div class="store"><p class="name">' + dealer.name + '</p><p class="description">' + dealer.street_address + ', ' + dealer.city + '/' + dealer.state_abbreviation + '</p><p class="phones">' + dealer.phones.join(' / ') + '</p>' + links + '</div></div>'
                });
            });

            jQuery(this).html((stores.length > 0 ? stores : '<p class="text-center"><em>Selecione um estado e uma cidade acima para ver as lojas exibidas aqui.</em></p>')).fadeIn(200);
        });
    }).change();
};