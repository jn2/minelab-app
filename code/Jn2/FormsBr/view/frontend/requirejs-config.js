var config = {
    config: {
        mixins: {
            'Magento_Ui/js/lib/validation/validator': {
                'Jn2_FormsBr/js/ui-validator-mixin': true
            },
            'mage/validation': {
                'Jn2_FormsBr/js/validator-mixin': true
            }
        }
    },
    deps: [
        'jquery',
        'Jn2_FormsBr/js/form-br',
        'Jn2_FormsBr/js/mask-br'
    ]
}