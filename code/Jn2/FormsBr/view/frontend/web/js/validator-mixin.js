define([
    'jquery',
    'Jn2_FormsBr/js/validators/vat-id-validator'
], function ($, cpfCnpj) {
    'use strict';

    return function (validator) {

        // Valida CNPJ
        $.validator.addMethod('validate-cnpj',
            function (value) {
                return cpfCnpj.validateCNPJ(value);
            },
            $.mage.__("CNPJ inválido.")
        );

        // Valida CPF
        $.validator.addMethod('validate-cpf',
            function (value) {
                return cpfCnpj.validateCPF(value);
            },
            $.mage.__("CPF inválido.")
        );


        // Valida CPF/CNPJ
        $.validator.addMethod('validate-vat-id',
            function (value) {
                return cpfCnpj.validate(value);
            },
            $.mage.__("CPF/CNPJ inválido.")
        );

        return validator;
    };
});

