<?php
namespace Jn2\FormsBr\Plugin\Customer\Block\Widget\Telephone;

class Plugin
{
    public function beforeToHtml(\Magento\Customer\Block\Widget\Telephone $subject) {
        $subject->setTemplate('Jn2_FormsBr::widget/telephone.phtml');
    }
}