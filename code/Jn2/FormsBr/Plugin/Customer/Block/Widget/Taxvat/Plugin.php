<?php
namespace Jn2\FormsBr\Plugin\Customer\Block\Widget\Taxvat;

class Plugin
{
    public function beforeToHtml(\Magento\Customer\Block\Widget\Taxvat $subject) {
        $subject->setTemplate('Jn2_FormsBr::widget/taxvat.phtml');
    }
}