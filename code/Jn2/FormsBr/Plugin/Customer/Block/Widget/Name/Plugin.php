<?php
namespace Jn2\FormsBr\Plugin\Customer\Block\Widget\Name;

class Plugin
{
    public function beforeToHtml(\Magento\Customer\Block\Widget\Name $subject) {
        $subject->setTemplate('Jn2_FormsBr::widget/name.phtml');
    }
}