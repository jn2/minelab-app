<?php

namespace Minelab\Menu\Block\View;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory as AttributeCollectionFactory;

class Data extends Template
{
    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $_httpContext;

    /**
     * \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $_attributeSetCollection;

    public function __construct(
        Context $context,
        array $data = [],
        \Magento\Framework\App\Http\Context $httpContext,
        ProductCollectionFactory $productCollectionFactory,
        AttributeCollectionFactory $attributeSetCollection
    ) {
        $this->_httpContext                 = $httpContext;
        $this->_productCollectionFactory    = $productCollectionFactory;
        $this->_attributeSetCollection      = $attributeSetCollection;

        parent::__construct($context, $data);
    }

    public function isCustomerLoggedIn()
    {
        return (bool)$this->_httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    public function getAllDetectors()
    {
        return $this->getAllProductsByAttr($this->getAttributeSetId('Detector'), '*');
    }

    public function getAllProductsByAttr($attrId, $attrToSelect = array('id', 'name'))
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->removeAllItems();

        $collection->addAttributeToFilter('attribute_set_id', $attrId);
        $collection->addAttributeToSelect($attrToSelect);
        $collection->setOrder('name','ASC');

        return $collection;
    }

    /**
     *
     * @param string $attributeSetName
     * @return int attributeSetId
     */
    public function getAttributeSetId($attributeSetName)
    {
        $attributeSetCollection = $this->_attributeSetCollection->create()
                                            ->addFieldToSelect('attribute_set_id')
                                                ->addFieldToFilter('attribute_set_name', $attributeSetName)
                                                    ->getFirstItem()
                                                        ->toArray();

        return isset($attributeSetCollection['attribute_set_id']) ? (int) $attributeSetCollection['attribute_set_id'] : FALSE;
    }

    public function makeLinkAccessories($productId)
    {
        $params['product_id'] = $productId;

        return $this->_storeManager->getStore()->getBaseUrl() . 'acessorios?' . http_build_query($params);
    }
}