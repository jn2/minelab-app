<?php

namespace Minelab\Accessories\Block;

class Display extends \Magento\Framework\View\Element\Template
{
    protected $_productRepository;
    protected $_productAttributeRepository;
    protected $_productCollectionFactory;
    protected $_storeManager;
    protected $_attributeSetCollection;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        array $data = []
    ) {
        $this->_productRepository           = $productRepository;
        $this->_productAttributeRepository  = $productAttributeRepository;
        $this->_productCollectionFactory    = $productCollectionFactory;
        $this->_storeManager                = $storeManager;
        $this->_attributeSetCollection      = $attributeSetCollection;

        parent::__construct($context, $data);
    }

    public function getRequestProductId()
    {
        return isset($_GET['product_id']) ? $_GET['product_id'] : NULL;
    }

    public function getRequestAccessoryTypeId()
    {
        return isset($_GET['accessory_type_id']) ? $_GET['accessory_type_id'] : NULL;
    }

    public function getAccessoriesWithFilter()
    {
        $collection = NULL;

        $prodId = $this->getRequestProductId();
        if( ! is_null($prodId))
            $collection = $this->getRelatedProductCollectionById($prodId);
        else
            $collection = $this->getAllProductsByAttr($this->getAttributeSetId('Accessory'), '*');

        $accessoryTypeId = $this->getRequestAccessoryTypeId();
        if( ! is_null($accessoryTypeId))
            $collection->addAttributeToFilter('accessory_type', $accessoryTypeId);

        return $collection;
    }

    public function getRelatedProductCollectionById($id)
    {
        $product = $this->_getProductById($id);

        if( ! is_null($product))
        {
            $collection = $product->getRelatedProductCollection();
            $collection->addAttributeToSelect('*');

            return $collection;
        }

        return NULL;
    }

    protected function _getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function getAllProductsByAttr($attrId, $attrToSelect = array('id', 'name'))
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->removeAllItems();

        $collection->addAttributeToFilter('attribute_set_id', $attrId);
        $collection->addAttributeToSelect($attrToSelect);

        return $collection;
    }

    public function getAllDetectorsToFilter()
    {
        return $this->getAllProductsByAttr($this->getAttributeSetId('Detector'))->getItems();
    }

    public function makeLinkAccessories($productId = NULL, $typeId = NULL)
    {
        $params = array();

        if( ! is_null($productId))
            $params['product_id'] = $productId;

        if( ! is_null($typeId))
            $params['accessory_type_id'] = $typeId;

        return $this->_storeManager->getStore()->getBaseUrl() . 'acessorios?' . http_build_query($params);
    }

    /**
     *
     * @param string $attributeSetName
     * @return int attributeSetId
     */
    public function getAttributeSetId($attributeSetName)
    {
        $attributeSetCollection = $this->_attributeSetCollection->create()
            ->addFieldToSelect('attribute_set_id')
            ->addFieldToFilter('attribute_set_name', $attributeSetName)
            ->getFirstItem()
            ->toArray();

        return isset($attributeSetCollection['attribute_set_id']) ? (int) $attributeSetCollection['attribute_set_id'] : FALSE;
    }

    public function getAccessoryTypes()
    {
        $types = array();
        $options = $this->_productAttributeRepository->get('accessory_type')->getOptions();

        foreach($options as $option)
        {
            if( ! empty($option->getValue()) && ! empty($option->getLabel()))
            $types[] = array('value' => $option->getValue(), 'label' => $option->getLabel());
        }

        return $types;
    }
}