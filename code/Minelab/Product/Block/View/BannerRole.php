<?php

namespace Minelab\Product\Block\View;

use Magento\Catalog\Block\Product\View\AbstractView;

class BannerRole extends AbstractView
{
    protected $imageAttributeBannerTop = 'product_banner_top';
    protected $featuresAttribute = 'product_features';
    protected $productImageFeaturesAttribute = 'product_image_features_spot';

    protected $featuresSourceImgAndText = array(
        'design_a_prova_dagua' => [
            'img'   => 'design_a_prova_dagua.png',
            'text'  => "A prova d'água"
        ],
        'elegante_e_leve' => [
            'img'   => 'elegante_e_leve.png',
            'text'  => "Elegante e leve"
        ],
        'audio_semfio_alta_velocidade' => [
            'img'   => 'audio_semfio_alta_velocidade.png',
            'text'  => "Audio Sem-fio de Alta velocidade"
        ],
        '8_perfis_pesquisa_customizados' => [
            'img'   => '8_perfis_pesquisa_customizados.png',
            'text'  => "8 Perfis de Pesquisa Customizados"
        ],
        'reconhecimento_alvo_rapido_preciso' => [
            'img'   => 'reconhecimento_alvo_rapido_preciso.png',
            'text'  => "Reconhecimento de Alvo Rápido e Preciso"
        ],
        'configuracoes_avancadas' => [
            'img'   => 'configuracoes_avancadas.png',
            'text'  => "Configurações Avançadas"
        ],
        'modo_deteccao_ouro' => [
            'img'   => 'modo_deteccao_ouro.png',
            'text'  => "Modo de Detecção de Ouro"
        ],
        '2_modos_deteccao' => [
            'img'   => '2_modos_deteccao.png',
            'text'  => "Dois Modos de Deteccao"
        ],
        'adaptador_haste_universal' => [
            'img'   => 'adaptador_haste_universal.png',
            'text'  => "Adaptador Haste Universal"
        ],
        'bateria_aa_recarregavel' => [
            'img'   => 'bateria_aa_recarregavel.png',
            'text'  => "Bateria AA recarregável"
        ],
        'duas_bobinas_acompanham_produto' => [
            'img'   => 'duas_bobinas_acompanham_produto.png',
            'text'  => "Duas bobinas acompanham o produto"
        ],
        'duas_bobinas_prova_dagua' => [
            'img'   => 'duas_bobinas_prova_dagua.png',
            'text'  => "Duas bobinas prova d'água, caixa de controle resistente à chuva"
        ],
        'indicador_opotunidade_de_ouro' => [
            'img'   => 'indicador_opotunidade_de_ouro.png',
            'text'  => "Indicador de opotunidade de ouro"
        ],
        'operacao_totalmente_automatica' => [
            'img'   => 'operacao_totalmente_automatica.png',
            'text'  => "Operacao totalmente automática"
        ],
        'super_sensivel' => [
            'img'   => 'super_sensivel.png',
            'text'  => "Super sensível"
        ],
        '6_modos_busca' => [
            'img'   => '6_modos_busca.png',
            'text'  => "Seis Modos de Busca"
        ],
        '8_temporizacoes' => [
            'img'   => '8_temporizacoes.png',
            'text'  => "Oito temporizações"
        ],
        'ajustes_audio' => [
            'img'   => 'ajustes_audio.png',
            'text'  => "Ajustes de áudio"
        ],
        'bateria_ions' => [
            'img'   => 'bateria_ions.png',
            'text'  => "Bateria de ions"
        ],
        'luz_fundo' => [
            'img'   => 'luz_fundo.png',
            'text'  => "Luz de fundo"
        ],
        'opcoes_automaticas_balanco' => [
            'img'   => 'opcoes_automaticas_balanco.png',
            'text'  => "Opções automáticas de balanço"
        ],
        'tecnologia' => [
            'img'   => 'tecnologia.png',
            'text'  => "Tecnologia"
        ],
        'audio_sem_fio' => [
            'img'   => 'audio_sem_fio.png',
            'text'  => "Áudio Sem Fio"
        ],
        'balanco_solo_selecionavel_cancelamento_ruido' => [
            'img'   => 'balanco_solo_selecionavel_cancelamento_ruido.png',
            'text'  => "Balanço solo selecionável cancelamento ruído"
        ],
        'bobina_prova_dagua_ate_1m' => [
            'img'   => 'bobina_prova_dagua_ate_1m.png',
            'text'  => "Bobina à prova d'água até 1m"
        ],
        'imunidade_ruidos' => [
            'img'   => 'imunidade_ruidos.png',
            'text'  => "Imunidade a ruídos"
        ],
        'localizacao_gps_mapeamento_pc' => [
            'img'   => 'localizacao_gps_mapeamento_pc.png',
            'text'  => "Localização GPS e mapeamento de PC"
        ],
        'transmissao_zero_voltagem' => [
            'img'   => 'transmissao_zero_voltagem.png',
            'text'  => "Transmissão de Zero Voltagem (ZVT)"
        ],
        'design_versatil' => [
            'img'   => 'design_versatil.png',
            'text'  => "Design versátil"
        ],
        'mais_sensivel' => [
            'img'   => 'mais_sensivel.png',
            'text'  => "Mais sensível"
        ],
        'tecnologia_multi_period_fast' => [
            'img'   => 'tecnologia_multi_period_fast.png',
            'text'  => "Tecnologia Multi Period Fast (MPF)"
        ],
        'a_mais_recente_discriminacao_feco' => [
            'img'   => 'a_mais_recente_discriminacao_feco.png',
            'text'  => "A Mais Recente Discriminação FeCo"
        ],
        'desenho_ergonomico' => [
            'img'   => 'desenho_ergonomico.png',
            'text'  => "Desenho Ergonômico"
        ],
        'menu_rapidos' => [
            'img'   => 'menu_rapidos.png',
            'text'  => "Menu Rápidos"
        ],
        'mapeamento_no_pc' => [
            'img'   => 'mapeamento_no_pc.png',
            'text'  => "Mapeamento no PC"
        ],
        'alta_visibilidade' => [
            'img'   => 'alta_visibilidade.png',
            'text'  => "Alta Visibilidade"
        ],
        'bateria_recarregavel' => [
            'img'   => 'bateria_recarregavel.png',
            'text'  => "Bateria recarregável"
        ],
        'bobina_10_slimline' => [
            'img'   => 'bobina_10_slimline.png',
            'text'  => "Bobina 10\" Slimline"
        ],
        'compacto_e_sem_montagem' => [
            'img'   => 'compacto_e_sem_montagem.png',
            'text'  => "Compacto & Sem Montagem"
        ],
        'modos_busca_predefinidos' => [
            'img'   => 'modos_busca_predefinidos.png',
            'text'  => "Modos de Busca predefinidos"
        ],
        'desempenho_superior' => [
            'img'   => 'desempenho_superior.png',
            'text'  => "Desempenho Superior"
        ],
        'easy_trak_automatico' => [
            'img'   => 'easy_trak_automatico.png',
            'text'  => "Easy-Trak Automático"
        ],
        'go_find_smartphone_app' => [
            'img'   => 'go_find_smartphone_app.png',
            'text'  => "Go-FIND Smartphone App"
        ],
        'visao_do_tesouro' => [
            'img'   => 'visao_do_tesouro.png',
            'text'  => "Visão do Tesouro"
        ],
        'alarme_de_perda' => [
            'img'   => 'alarme_de_perda.png',
            'text'  => "Alarme de perda"
        ],
        'alta_visibilidade2' => [
            'img'   => 'alta_visibilidade2.png',
            'text'  => "Alta visibilidade"
        ],
        'coldre_incluido' => [
            'img'   => 'coldre_incluido.png',
            'text'  => "Coldre incluído"
        ],
        'design_a_prova_de_respingos' => [
            'img'   => 'design_a_prova_de_respingos.png',
            'text'  => "Design à prova de respingos"
        ],
        'indicacao_sonora' => [
            'img'   => 'indicacao_sonora.png',
            'text'  => "Indicação sonora"
        ],
        'a_prova_dagua_ate_3m_10pes' => [
            'img'   => 'a_prova_dagua_ate_3m_10pes.png',
            'text'  => "À prova d'água até 3m (10 pés)"
        ],
        'coldre_e_cordao_de_seguranca_incluido' => [
            'img'   => 'coldre_e_cordao_de_seguranca_incluido.png',
            'text'  => "Coldre e cordão de segurança incluído"
        ],
        'indicacao_sonora_e_por_vibracao' => [
            'img'   => 'indicacao_sonora_e_por_vibracao.png',
            'text'  => "Indicação sonora e por vibração"
        ],
        'sensibilidade_ajustavel' => [
            'img'   => 'sensibilidade_ajustavel.png',
            'text'  => "Sensibilidade ajustável"
        ],
        'tom_de_identificacao_de_ferro' => [
            'img'   => 'tom_de_identificacao_de_ferro.png',
            'text'  => "Tom de identificação de ferro"
        ],
        'leve' => [
            'img'   => 'leve.png',
            'text'  => "Leve"
        ],
        '6_perfis_pesquisa_customizados' => [
            'img'   => '6_perfis_pesquisa_customizados.png',
            'text'  => "6 Perfis de Pesquisa Customizados"
        ],
        'multi_iq' => [
            'img'   => 'multi_iq.png',
            'text'  => "Multi-IQ"
        ],
    );

    public function getFromTop()
    {
        return $this->getBannerTopByProduct($this->getProduct());
    }

    public function getBannerTopByProduct($product)
    {
        return $this->getCustomAttrImgFile($this->imageAttributeBannerTop, $product);
    }

    public function getFeatures()
    {
        return $this->getFeaturesByProduct($this->getProduct());
    }

    public function getFeaturesByProduct($product)
    {
        $value = $product->getProductFeatures();

        $attr = $product->getResource()->getAttribute($this->featuresAttribute);
        $features = array();

        if( ! is_null($value) && $attr->usesSource())
        {
            $value = $attr->getSource()->getOptionText($value);

            $_features = is_array($value) ? $value : array($value);

            foreach ($_features as $feature)
            {
                if(isset($this->featuresSourceImgAndText[$feature]))
                    $features[] = $this->featuresSourceImgAndText[$feature];
            }
        }

        return $features;
    }

    public function getImgFeaturesSpotlight()
    {
        return $this->getImgFeaturesSpotlightByProduct($this->getProduct());
    }

    public function getImgFeaturesSpotlightByProduct($product)
    {
        return $this->getCustomAttrImgFile($this->productImageFeaturesAttribute, $product);
    }

    protected function getCustomAttrImgFile($attr, $product)
    {
        $value = $product->getData($attr);

        return is_null($value) || $value == 'no_selection' ? NULL : str_replace('\\', '/', $this->_imageHelper->init($product, $attr)->setImageFile($value)->getUrl());
    }
}