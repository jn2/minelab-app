<?php

namespace Minelab\Product\Block\View;

use Magento\Catalog\Model\Product;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Review\Model\ResourceModel\Review\Product\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class Data extends Template
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var CollectionFactory
     */
    protected $_collectionReviewFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * \Magento\Framework\App\ObjectManager
     */
    protected $_objectManager;

    public function __construct(
        Context $context,
        array $data = [],
        Registry $registry,
        CollectionFactory $collectionReviewFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->registry = $registry;

        $this->_collectionReviewFactory = $collectionReviewFactory;

        $this->_storeManager = $storeManager;

        parent::__construct($context, $data);

        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function getLinkVideo()
    {
        return $this->getProduct()->getProductLinkVideo();
    }

    public function getSrcEmbedVideo()
    {
        $link = $this->getLinkVideo();

        if( ! empty($link))
            return (stripos($link,'youtu') === FALSE) ? $this->getIdByLinkVimeo($link) : $this->getIdByLinkYoutube($link);

        return NULL;
    }

    protected function getIdByLinkYoutube($link)
    {
        preg_match('/(http(s|):|)\/\/(www\.|)yout(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $link, $matches);

        return empty($matches[6]) ? NULL : 'https://www.youtube.com/embed/' . $matches[6]. '?autoplay=0';
    }

    protected function getIdByLinkVimeo($link)
    {
        preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $link, $matches);

        return empty($matches[3]) ? NULL : 'https://player.vimeo.com/video/' . $matches[3];
    }

    public function getSpecifications()
    {
        return $this->getProduct()->getProductSpecifications();
    }

    public function getReviews()
    {
        $product = $this->getProduct();
        $collection = $this->_collectionReviewFactory->create()->addStoreFilter(
                    $this->_storeManager->getStore()->getId()
                )->addStatusFilter(
            \Magento\Review\Model\Review::STATUS_APPROVED
                )->addEntityFilter(
                    $product->getId()
                )->setDateOrder();

        return $collection->getData();
    }

    /**
     * @return Product
     */
    private function getProduct()
    {
        if (is_null($this->product)) {
            $this->product = $this->registry->registry('product');

            if (!$this->product->getId()) {
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }

        return $this->product;
    }

    /*public function getParticulars()
    {
        return $this->getContentIdByProductParticulars();

        return $this->getEntityByProductSku();
    }

    private function getEntityIdByContentId($contentId)
    {
        $resource =  $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('blackbird_contenttype_entity');
        $result = $connection->fetchAll("Select * FROM $tableName WHERE ct_id = '" . $contentId . "'");

        return count($result) > 0 ? $result[0] : NULL;
    }

    private function getContentIdByProductParticulars()
    {
        $contentCollection = $this->_objectManager->create('Blackbird\ContentManager\Model\ResourceModel\Content\CollectionFactory');
        $collection = $contentCollection->create()->addContentTypeFilter('product_particulars');

        $result = $collection->getFirstItem()->getData();

        return empty($result) ? NULL : $result['ct_id'];
    }

    private function getEntityByProductSku()
    {
        $resource =  $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('blackbird_contenttype_entity_text');
        $result = $connection->fetchAll("Select * FROM $tableName WHERE value = '" . $this->getProduct()->getSku() . "'");

        return count($result) > 0 ? $result[0] : NULL;
    }*/
}