<?php

namespace Minelab\Detectors\Block\View;

class Filter extends \Magento\Framework\View\Element\Template
{
    protected $_productAttributeRepository;
    protected $_productCollectionFactory;
    protected $_attributeSetCollection;
    protected $_storeManager;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->_productAttributeRepository  = $productAttributeRepository;
        $this->_attributeSetCollection      = $attributeSetCollection;
        $this->_productCollectionFactory    = $productCollectionFactory;
        $this->_storeManager                = $storeManager;

        parent::__construct($context, $data);
    }

    public function getRequestObjectiveId()
    {
        return isset($_GET['objective_id']) ? $_GET['objective_id'] : NULL;
    }

    public function getRequestLevelId()
    {
        return isset($_GET['level_id']) ? $_GET['level_id'] : NULL;
    }

    public function getRequestWaterproof()
    {
        return isset($_GET['waterproof']) ? $_GET['waterproof'] == 1 : FALSE;
    }

    public function getObjectivesOptions()
    {
        return $this->getAttrOptions('objective');
    }

    public function getLevelsOptions()
    {
        return $this->getAttrOptions('level');
    }

    protected function getAttrOptions($attrCode)
    {
        $options = array();

        foreach($this->_productAttributeRepository->get($attrCode)->getOptions() as $option)
        {
            if( ! empty($option->getValue()) && ! empty($option->getLabel()))
                $options[] = array('value' => $option->getValue(), 'label' => $option->getLabel());
        }

        return $options;
    }

    public function getDetectorsWithFilter()
    {
        $collection = $this->getAllProductsByAttr($this->getAttributeSetId('Detector'), '*');

        $objectiveId = $this->getRequestObjectiveId();
        if( ! is_null($objectiveId))
            $collection->addAttributeToFilter(
                'objective',
                array(
                    array('finset' => array($objectiveId))
                )
            );

        $waterproof = $this->getRequestWaterproof();
        if($waterproof)
            $collection->addAttributeToFilter('waterproof', 1);

        $levelId = $this->getRequestLevelId();
        if( ! is_null($levelId))
            $collection->addAttributeToFilter('level', $levelId);

        return $collection;
    }

    /**
     *
     * @param string $attributeSetName
     * @return int attributeSetId
     */
    public function getAttributeSetId($attributeSetName)
    {
        $attributeSetCollection = $this->_attributeSetCollection->create()
            ->addFieldToSelect('attribute_set_id')
            ->addFieldToFilter('attribute_set_name', $attributeSetName)
            ->getFirstItem()
            ->toArray();

        return isset($attributeSetCollection['attribute_set_id']) ? (int) $attributeSetCollection['attribute_set_id'] : FALSE;
    }

    public function getAllProductsByAttr($attrId, $attrToSelect = array('id', 'name'))
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->removeAllItems();

        $collection->addAttributeToFilter('attribute_set_id', $attrId);
        $collection->addAttributeToSelect($attrToSelect);

        return $collection;
    }

    public function makeLinkDetectors($objectiveId = NULL, $waterproof = FALSE, $levelId = NULL)
    {
        $params = array();

        if( ! is_null($objectiveId))
            $params['objective_id'] = $objectiveId;

        if($waterproof === TRUE)
            $params['waterproof'] = 1;

        if( ! is_null($levelId))
            $params['level_id'] = $levelId;

        return $this->_storeManager->getStore()->getBaseUrl() . 'detectores-de-metal?' . http_build_query($params);
    }
}