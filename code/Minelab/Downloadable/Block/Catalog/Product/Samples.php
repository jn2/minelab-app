<?php

namespace Minelab\Downloadable\Block\Catalog\Product;

class Samples extends \Magento\Downloadable\Block\Catalog\Product\Samples
{
    public function hasSamples()
    {
        return method_exists($this->getProduct()->getTypeInstance(),'hasSamples') ? parent::hasSamples() : FALSE;
    }
}