<?php
namespace Minelab\Newsletters\Block\Newsletter\Adminhtml\Template\Grid\Renderer;
use Magento\Framework\DataObject;
class Customercountry extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    public function render(\Magento\Framework\DataObject $row)
    {
        if($row->getData('type')==1){
            return ($row->getData('c_country')!=''?$row->getData('c_country'):'----');
        }else{
            return ($row->getData('country')!=''?$row->getData('country'):'----');
        }
    }
}